<!-- General JS Scripts -->
<script src="<?php echo base_url();?>assets/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="<?php echo base_url();?>assets/bundles/chartjs/chart.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/apexcharts/apexcharts.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url();?>assets/bundles/jqvmap/dist/maps/jquery.vmap.indonesia.js"></script>
 <!--start Datatable with export js-->   
<script src="<?php echo base_url();?>assets/bundles/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/bundles/datatables/export-tables/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets/js/page/datatables.js"></script>
<!--End of Datatable with export js--> 

<!--start editable datatable js  -->
<script src="<?php echo base_url()?>assets/bundles/editable-table/mindmup-editabletable.js"></script>

<script src="<?php echo base_url()?>assets/js/page/editable-table.js"></script>
<!--End editable datatable js  -->


<!-- Page Specific JS File -->
<script src="<?php echo base_url();?>assets/js/page/index2.js"></script>
<script src="<?php echo base_url();?>assets/js/page/todo.js"></script>

<script src="<?php echo base_url()?>assets/bundles/prism/prism.js"></script>
<!-- Template JS File -->
<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
<!-- Master JS File -->
<script src="<?php echo base_url();?>assets/js/master.js"></script>
<!-- multiselect JS file -->
<script src="<?php echo base_url();?>assets/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url();?>assets/js/init-multiselect.js"></script>

<!-- bootstrap min JS file -->
<script src="<?php echo base_url();?>assets/js/bootstrap-3.3.2.min.js"></script>

<!-- bootstrap toogle button -->
<script src="<?php echo base_url();?>assets/js/bootstrap4-toggle.min.js"></script>

<!-- Ckeditor library -->
<script src="<?php echo base_url();?>assets/bundles/ckeditor/ckeditor.js"></script>

<!-- <script src="https://cdn.ckeditor.com/4.13.0/standard-all/ckeditor.js"></script> -->
<script src="<?php echo base_url();?>assets/js/init-ckeditor.js"></script>

<!-- Drag and Drop image -->
<script src="<?php echo base_url();?>assets/js/dropzone.js"></script>

<!-- Gijgo Datepicker -->
<script src="<?php echo base_url();?>assets/js/gijgo-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/init-datepicker.js"></script>

<script type="text/javascript">
	/*http://www.soundjay.com/misc/sounds/bell-ringing-01.mp3*/
var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', '<?=base_url('assets/deduction.mp3');?>');
    
    audioElement.addEventListener('ended', function() {
        this.play();
    }, false);
    
    function order_bell() {
    	audioElement.play();
    }

    $(document).ready(function(){
    	deleteAllCookies();
		$('.pay_status').change(function(){
			var id = $(this).attr('id');
			var status = $(this).val();
			var txn_id = prompt("Please enter Transaction Number");
			  if (txn_id != null) {
			    $.ajax({
					url: base_url+'wallet_transactions/change_status',
					type: 'post',
					data:{id : id, status : status, txn_id : txn_id},
					success: function(data){
						window.location.reload();
					}
				});
			  }
		});
    });

    function deleteAllCookies() {
        var cookies = document.cookie.split(";");

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
        return true;
    }
</script>