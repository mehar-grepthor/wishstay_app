<div class="main-sidebar sidebar-style-2">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand">
						<a href="index.html"> <img alt="image" src="<?php echo base_url()?>assets/img/logo.png" class="header-logo" /> 
                            <!--<span class="logo-name">Aegis</span>-->
						</a>
					</div>
					<div class="sidebar-user">
						<div class="sidebar-user-picture">
							<img alt="image" src="<?php echo base_url()?>assets/img/userbig.png">
						</div>
						<div class="sidebar-user-details">
							<div class="user-name"><?php echo $user->email;?></div>
							<div class="user-role"><?php echo $user->first_name.''.$user->last_name;?></div>
						</div>
					</div>
					<ul class="sidebar-menu">
						<li class="menu-header">Main</li>
						<li class="dropdown active"><a href="<?php echo base_url('dashboard');?>" class="nav-link "><i
									data-feather="monitor"></i><span>Dashboard</span>
							</a>
							
						</li>
					
						<!-- Quiz -->
							<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="copy"></i><span>Master Data</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('category/r');?>">Categories</a></li>
        								<li><a class="nav-link" href="<?php echo base_url('products/r');?>">Products</a></li>
    									
    				            </ul>
    						</li>
    						
    						<li class="dropdown "><a href="<?php echo base_url('wallet_transactions/list');?>" class="nav-link "><i data-feather="monitor"></i><span>Transactions</span>
    							</a>
    						</li>
						
    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="copy"></i><span>Employees</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('employee/r');?>">Add Employee</a></li>
        								<li><a class="nav-link" href="<?php echo base_url('emp_list/executive')?>">Executives</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('role/r');?>">Add Role</a></li>
    				            </ul>
    						</li>

    						<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="copy"></i><span>Settings</span></a>
    							<ul class="dropdown-menu">
        								<li><a class="nav-link" href="<?php echo base_url('settings/r');?>">Site Settings</a></li>
    									<li><a class="nav-link" href="<?php echo base_url('sliders/r');?>">Manage Sliders</a></li>
    				            </ul>
    						</li>
					</ul>
				</aside>
			</div>