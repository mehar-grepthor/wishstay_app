<?php if($type == 'category'){?>
 <!--Edit Category -->
    <div class="row">
    <div class="col-12">
        <h4>Edit Category</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('category/u');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">

                <div class="form-group row">
                    <div class="form-group col-md-6">
                        <label>Category Name</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $category['name'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $category['id'] ; ?>">

					<div class="form-group col-md-6">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/category_image/category_<?php echo $category['id']; ?>.jpg" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/category_image/category_<?php echo $category['id']; ?>.jpg" style="width: 200px;" />

                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Description</label>
                         <textarea id="cat_terms"  class="ckeditor" name="desc" rows="10" data-sample-short>
                            <?php echo $category['desc'];?>
                        </textarea>
                          <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                        <div class="invalid-feedback">Give some Description</div>
                    </div>

                 
                    <div class="form-group col-md-12">
                        <!--                             <button type="submit" name="upload" id="upload" value="Apply" class="btn btn-primary mt-27 ">Update</button> -->
                        <button class="btn btn-primary mt-27 ">Update</button>

                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<?php } elseif($type == 'products'){?>
 <div class="row">
                    <div class="col-12">
                        <h4>Edit Product</h4>
                        <form class="needs-validation" novalidate="" action="<?php echo base_url('products/u');?> " method="post" enctype="multipart/form-data">
                            <div class="card-header">
                                <div class="form-row">
                                     <div class="form-group col-md-4">
                        <label>Product Name</label>
                        <input type="text" name="name" class="form-control" required="" value="<?php echo $products['name'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $products['id'] ; ?>">
					 <div class="form-group col-md-4">
                                <label>Category</label>
                                <!-- <input type="file" class="form-control" required="">-->
                                <select class="form-control" name="cat_id" required="">
                                    <option value="0" selected disabled>select</option>
                                    <?php foreach ($categories as $category):?>
                                      <option value="<?php echo $category['id'];?>" <?php echo ($category['id'] == $products['cat_id'])? 'selected': '';?>><?php echo $category['name']?></option>
                                    <?php endforeach;?>
                                </select>
                                <div class="invalid-feedback">Select Category Name?</div>
                            </div>
                            
					<div class="form-group col-md-4">
                        <label>Upload Image</label>
                        <input type="file" id='input1' name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/product_image/product_<?php echo $products['id']; ?>.jpg" style="width: 200px;">
                        <br>
                        <img id="blah" src="<?php echo base_url(); ?>uploads/product_image/product_<?php echo $products['id']; ?>.jpg" style="width: 200px;" />

                        <div class="invalid-feedback">Upload Image?</div>
                    </div>
                          <div class="form-group col-md-3">
                        <label>Price</label>
                        <input type="text" name="price" class="form-control" required="" value="<?php echo $products['price'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                         
                              <div class="form-group col-md-3">
                        <label>Discount Price</label>
                        <input type="text" name="discount_price" class="form-control" required="" value="<?php echo $products['discount_price'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                              <div class="form-group col-md-3">
                        <label>Quantity</label>
                        <input type="text" name="qty" class="form-control" required="" value="<?php echo $products['qty'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Size</label>
                        <input type="text" name="size" class="form-control" required="" value="<?php echo $products['size'];?>">
                        <div class="invalid-feedback">Enter Valid Category Name?</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Description</label>
                         <textarea id="cat_terms"  class="ckeditor" name="desc" rows="10" data-sample-short>
                            <?php echo $products['desc'];?>
                        </textarea>
                          <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                        <div class="invalid-feedback">Give some Description</div>
                    </div>
                                  
                                    <div class="form-group col-md-6">
                                        <button class="btn btn-primary mt-27 ">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    

<?php }?>