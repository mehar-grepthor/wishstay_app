<?php 

require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Product extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('cart_model');
    }
    /**
     * @author trupti
     * @desc To get list category
     * @param string $target
     */
    
    public function categories_get($target = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->category_model->order_by('name', 'ASC')->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/category_image/category_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->category_model->where('id', $target)->get();
            $data['image'] = base_url().'uploads/category_image/category_'.$data['id'].'.jpg';
          
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    
    /**
     * @author trupti
     * @desc To get list product
     * @param string $target
     */
    
    public function product_get($target = '') {
        //$this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if(empty($target)){
            $data = $this->product_model->get_all();
            if(! empty($data)){
                for ($i = 0; $i < count($data) ; $i++){
                    $data[$i]['image'] = base_url().'uploads/product_image/image_'.$data[$i]['id'].'.jpg';
                }
            }
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }else{
            $data = $this->product_model->with_categories('fields:name,id')->with_product_image()->where('id', $target)->get();
            $data['image'] = base_url().'uploads/product_image/image_'.$data['id'].'.jpg';
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }
    }
    public function cart_post($type = 'r'){
        $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
        if($type == 'c'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->cart_model->rules);
            if ($this->form_validation->run() == false) {
                $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $id = $this->cart_model->insert([
                    'user_id' => $token_data->id,
                    'product_id' => $this->input->post('product_id'),
                    'qty' => (! empty($this->input->post('qty')))? $this->input->post('qty') : 0
                ]);
                $this->set_response_simple($id, 'Success..!', REST_Controller::HTTP_CREATED, TRUE);
            }   
        }elseif ($type == 'r'){
            $data = $this->cart_model->get_all();
            $this->set_response_simple(($data == FALSE)? FALSE : $data, 'Success..!', REST_Controller::HTTP_OK, TRUE);
        }elseif($type == 'u'){
            $_POST = json_decode(file_get_contents("php://input"), TRUE);
            $this->form_validation->set_rules($this->cart_model->rules);
            if ($this->form_validation->run() == FALSE) {
            $this->set_response_simple(validation_errors(), 'Validation Error', REST_Controller::HTTP_NON_AUTHORITATIVE_INFORMATION, FALSE);
            } else {
                $data= $this->cart_model->update([
                'id' => $this->input->post('id'),
                'user_id' => $token_data->id,
                'product_id' => $this->input->post('product_id'),
                'qty' => $this->input->post('qty'),
            ], 'id');
                $this->set_response_simple(($data == FALSE) ? FALSE : $data, 'Updated..!', REST_Controller::HTTP_ACCEPTED, TRUE);
        }
    }
    }
    

}
?>