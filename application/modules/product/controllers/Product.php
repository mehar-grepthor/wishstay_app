<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * @author Mehar
 *         Admin module
 */
class Product extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');

        // if( ! $this->ion_auth_acl->has_permission('access_admin') )
        // redirect('admin/dashboard');
         
        $this->load->model('category_model');
        $this->load->model('product_model');
        $this->load->model('product_image_model');
        
    }

    public function index()
    {
        redirect('admin/dashboard');
    }

   
  
    /**
     * Product crud
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function products($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('sub_category'))
         redirect('admin'); */
        if ($type == 'c') {
            $this->form_validation->set_rules($this->product_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'product Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->products('r');
            } else {
                $id = $this->product_model->insert([
                    'name' => $this->input->post('name'),
                    'cat_id' => $this->input->post('cat_id'),
                    'price' => $this->input->post('price'),
                    'discount_price' => $this->input->post('discount_price'),
                    'qty' => $this->input->post('qty'),
                    'size' => $this->input->post('size'),
                    'desc' => $this->input->post('desc'),
                ]);
                if ($_FILES['file']['name'] !== '') {
                    foreach ($_FILES['file']['tmp_name'] as $key => $value){
                        // first insert one recored into product images table it gives insert_id
                        $img_id = $this->product_image_model->insert([
                            'product_id' =>$id
                        ]);
                        //with the reference of  insert id place image in folder uploads/product_image/image_$image_id.jpg
                        move_uploaded_file($_FILES['file']['tmp_name'][$key], "./uploads/product_image/product_$img_id.jpg");
                    }
                }
                
                redirect('products/r', 'refresh');
            }
        } elseif ($type == 'r') {
           // echo "hi";
            $this->data['title'] = 'Product';
            $this->data['content'] = 'product/products';
            $this->data['categories'] = $this->category_model->get_all();
            $this->data['sub_categories'] = $this->product_model->order_by('id', 'DESC')->get_all();
            $this->data['product_images'] = $this->product_image_model->get();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
            
            $this->form_validation->set_rules($this->product_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
               /* $this->product_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'cat_id' => $this->input->post('cat_id'),
                    'price' => $this->input->post('price'),
                    'discount_price' => $this->input->post('discount_price'),
                    'qty' => $this->input->post('qty'),
                    'size' => $this->input->post('size'),
                    'desc' => $this->input->post('desc')
                ], $this->input->post('id'));
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "product", $this->input->post('id'), '', 'no');
                    }*/ if ($_FILES['file']['name'] !== '') {
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $id = $this->product_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'cat_id' => $this->input->post('cat_id'),
                    'price' => $this->input->post('price'),
                    'discount_price' => $this->input->post('discount_price'),
                    'qty' => $this->input->post('qty'),
                    'size' => $this->input->post('size'),
                    'desc' => $this->input->post('desc'),
                    'image' => $path,
                ],$this->input->post('id'));
                $this->file_up("file", "product", $id, '', 'no', '.' . $ext);
            }
                redirect('products/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->product_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Product';
            $this->data['content'] = 'product/edit';
            $this->data['type'] = 'products';
            $this->data['products'] = $this->product_model->where('id',$this->input->get('id'))->get();
            $this->data['categories'] = $this->category_model->order_by('id', 'DESC')->get_all();
            
           // print_array( $this->data['products']);
            $this->_render_page($this->template, $this->data);
        }
    }
    /**
     * Category crud
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function category($type = 'r'){
        if($type == 'c'){
            $this->form_validation->set_rules($this->category_model->rules);
            if(empty($_FILES['file']['name'])){
                $this->form_validation->set_rules('file','Category Image','required');
            }
           
            if($this->form_validation->run()== FALSE){
                $this->category('r');
            }else{
                $id = $this->category_model->insert([
                    'name' => $this->input->post('name'),
                    'desc' =>$this->input->post('desc')
                ]);
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "category", $id, '', 'no');
                redirect('category/r', 'refresh');
            }
        }elseif ($type == 'r'){
            $this->data['title'] = 'Category';
            $this->data['content'] = 'product/category';
            $this->data['categories'] = $this->category_model->order_by('id','DESC')->get_all();
           $this->_render_page($this->template,$this->data);
        }elseif ($type == 'u'){
         
            $this->form_validation->set_rules($this->category_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->category_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc')
                ], $this->input->post('id'));
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "category", $this->input->post('id'), '', 'no');
                }
                redirect('category/r', 'refresh');
            }
        }elseif ($type == 'd'){
            $this->category_model->delete(['id' =>$this->input->post('id')]);
        }elseif ($type == 'edit'){
            $this->data['title'] = 'Edit Category';
            $this->data['content'] = 'product/edit';
            $this->data['type'] = 'category';
            $this->data['category'] = $this->category_model->where('id',$this->input->get('id'))->get();
            $this->data['i'] = $this->category_model->where('file',$this->input->get('file'))->get();
            $this->_render_page($this->template, $this->data);
        }
    }
    
   
    
}