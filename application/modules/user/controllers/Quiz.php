<?php

require APPPATH . '/libraries/MY_REST_Controller.php';
require APPPATH . '/vendor/autoload.php';

use Firebase\JWT\JWT;

class Quiz extends MY_REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->model('question_model');
        $this->load->model('plan_model');
        $this->load->model('sub_category_model');
        $this->load->model('lobby_model');
        $this->load->model('room_model');
        $this->load->model('room_user_model');
    }
    
    public function lobby_post($type = 'join') {
        if($type == 'join'){
            $_POST = json_decode(file_get_contents('php://input'), TRUE);
            $token_data = $this->validate_token($this->input->get_request_header('X_AUTH_TOKEN'));
            $cat_id = $this->input->post('cat_id');
            $plan_id = $this->input->post('plan_id');
            $this->search($token_data, $cat_id, $plan_id);
            $this->set_response_simple(NULL, 'Entered into lobby', REST_Controller::HTTP_CREATED, TRUE);
        }elseif ($type == 'expire'){
            $delete_from_lobby = $$this->lobby_model->where('user_id', $search_lobby[$is_user_id_there]['user_id'])->delete();
            $this->set_response_simple($delete_from_lobby, 'Lobby cleared', REST_Controller::HTTP_CREATED, TRUE);
        }
        
    }
    
    public function search($token_data = NULL, $cat_id = NULL, $plan_id = NULL){
        $search_lobby = $this->lobby_model->where(['cat_id' => $cat_id, 'plan_id' => $plan_id, 'status' => 2])->get_all();
        if(empty($search_lobby)){
            $search_room = $this->room_model->where(['cat_id' => $cat_id, 'plan_id' => $plan_id, 'room_capacity >=' =>2, 'room_capacity <=' => 5, 'status' => 1])->get_all();
            if(empty($search_room)){
                $lobby_id = $this->lobby_model->insert([ 'user_id' => $token_data->id, 'cat_id' => $cat_id, 'plan_id' => $plan_id]);
            }else{
                $this->room_user_model->insert(['room_id' => $search_room[0]['id'], 'user_id' => $token_data->id]);
                $this->room_model->update(['id' => $search_room[0]['id'], 'room_capacity' => $search_room[0]['room_capacity'] + 1], 'id');
            }
        }else {
            $is_user_id_there = array_search($token_data->id, array_column($search_lobby, 'user_id'));
            if($is_user_id_there !== FALSE){
                $this->lobby_model->where('user_id', $search_lobby[$is_user_id_there]['user_id'])->delete();
                $this->search($token_data, $cat_id, $plan_id);
            }else{
                $room_id = $this->room_model->insert(['cat_id' => $cat_id, 'plan_id' =>$plan_id, 'started_at' => $search_lobby[0]['entered_at'], 'room_capacity' => 2]);
                if(! empty($room_id)){
                    $this->room_user_model->insert([['room_id' => $room_id, 'user_id' => $search_lobby[0]['user_id']], ['room_id' => $room_id, 'user_id' => $token_data->id]]);
                }
                $this->lobby_model->where('user_id', $search_lobby[$is_user_id_there]['user_id'])->delete();
            }
        }
    }
}

