<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * @author Mehar
 *         Admin module
 */
class Mitra extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->template = 'template/admin/main';
        if (! $this->ion_auth->logged_in()) // || ! $this->ion_auth->is_admin()
            redirect('auth/login');

        // if( ! $this->ion_auth_acl->has_permission('access_admin') )
        // redirect('admin/dashboard');
         
        $this->load->model('scheme_model');
        $this->load->model('product_model');
    }

    public function index()
    {
        redirect('admin/dashboard');
    }

    /**
     * category crud
     *
     * @author trupti
     * @param string $type
     * @param string $target
     */
   
    public function category($type = 'r')
    {
        /* if (! $this->ion_auth_acl->has_permission('category'))
         redirect('admin'); */
        
        if ($type == 'c') {
            $this->form_validation->set_rules($this->category_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'Category Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->category('r');
            } else {
                $id = $this->category_model->insert([
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'terms' => $this->input->post('terms'),
                ]);
               
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "category", $id, '', 'no');
                redirect('category/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Category';
            $this->data['content'] = 'master/category';
            $this->data['categories'] = $this->category_model->with_brands('fields:id, name')->with_services('fields:name,desc')->with_amenities('fields:name, desc')->with_categories_services('fields:service_id')->get_all();
            $this->data['services'] = $this->service_model->order_by('id', 'DESC')->get_all();
            $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        } elseif ($type == 'u') {
            
            $this->form_validation->set_rules($this->category_model->rules);
            if ($this->form_validation->run() == FALSE) {
                $this->category('r');
            } else {
                $this->category_model->update([
                    'id' => $this->input->post('id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc'),
                    'terms' => $this->input->post('terms')
                ],  $this->input->post('id'));
                $this->db->delete('categories_services', ['cat_id' => $this->input->post('id')]);
                if(! empty($this->input->post('service_id'))){foreach ($this->input->post('service_id') as $sid){
                    $this->db->insert('categories_services', ['cat_id' => $this->input->post('id'), 'service_id' => $sid]);
                }}
                
                $this->db->delete('categories_brands', ['cat_id' => $this->input->post('id')]);
                if(! empty($this->input->post('brand_id'))){foreach ($this->input->post('brand_id') as $bid){
                    $this->db->insert('categories_brands', ['cat_id' => $this->input->post('id'), 'brand_id' => $bid]);
                }}
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    //$this->file_up("file", "category", $this->input->post('id'), '', 'no');
                    unlink('uploads/' . 'category' . '_image/' . 'category' . '_' . $this->input->post('id') . '.jpg');
                    move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . 'category' . '_image/' . 'category' . '_' . $this->input->post('id') . '.jpg');
                }
                redirect('category/r', 'refresh');
            }
        } elseif ($type == 'd') {
            echo $this->category_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Category';
            $this->data['content'] = 'master/edit';
            $this->data['type'] = 'category';
            $this->data['category'] = $this->category_model->where('id',$this->input->get('id'))->get();
            $this->data['i'] = $this->category_model->where('file',$this->input->get('file'))->get();
            $this->data['categories'] = $this->category_model->with_brands('fields: id, name')->with_services('fields:id, name')->where('id', $this->input->get('id'))
            ->get();
            $this->data['services'] = $this->service_model->get_all();
            $this->data['brands'] = $this->brand_model->order_by('id', 'DESC')->get_all();
            //print_array($this->data['categories']);
            $this->_render_page($this->template, $this->data);
        }
    }
    /**
     * Product crud
     *
     * @author Trupti
     * @param string $type
     * @param string $target
     */
    public function product($type = 'r')
    {
        
        /* if (! $this->ion_auth_acl->has_permission('sub_category'))
         redirect('admin'); */
        
        if ($type == 'c') {
            
            $this->form_validation->set_rules($this->product_model->rules);
            if (empty($_FILES['file']['name'])) {
                $this->form_validation->set_rules('file', 'product Image', 'required');
            }
            if ($this->form_validation->run() == FALSE) {
                $this->product('r');
            } else {
                $id = $this->product_model->insert([
                    'product_id' => $this->input->post('product_id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc')
                ]);
                $path = $_FILES['file']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $this->file_up("file", "product", $id, '', 'no');
                redirect('product/r', 'refresh');
            }
        } elseif ($type == 'r') {
            $this->data['title'] = 'Product';
            $this->data['content'] = 'admin/mitra/products';
            $this->data['categories'] = $this->scheme_model->get_all();
            $this->data['sub_categories'] = $this->product_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
            //echo json_encode($this->data);
        } elseif ($type == 'u') {
            
            $this->form_validation->set_rules($this->product_model->rules);
            if ($this->form_validation->run() == FALSE) {
                echo validation_errors();
            } else {
                $this->product_model->update([
                    'product_id' => $this->input->post('product_id'),
                    'name' => $this->input->post('name'),
                    'desc' => $this->input->post('desc')
                ], $this->input->post('id'));
                if ($_FILES['file']['name'] !== '') {
                    $path = $_FILES['file']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $this->file_up("file", "product", $this->input->post('id'), '', 'no');
                }
                redirect('product/r', 'refresh');
            }
        } elseif ($type == 'd') {
            $this->product_model->delete(['id' => $this->input->post('id')]);
        }elseif($type == 'edit'){
            $this->data['title'] = 'Edit Product';
            $this->data['content'] = 'mitra/edit';
            $this->data['type'] = 'product';
            $this->data['sub_categories']=$this->product_model->order_by('id', 'DESC')->where('id', $this->input->get('id'))->get();
            $this->data['categories'] = $this->scheme_model->order_by('id', 'DESC')->get_all();
            $this->_render_page($this->template, $this->data);
        }
    }
    
    
   
    
}