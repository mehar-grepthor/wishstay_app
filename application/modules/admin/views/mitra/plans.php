<!--Add Plan And its list-->
<div class="row">
    <div class="col-12">
        <h4>Add Plans</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('plans/c');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">
                <div class="form-group row">
                    <div class="form-group col-md-3">
                        <label>Price</label>
                        <input type="number" name="price" required="" placeholder="Price" value="<?php echo set_value('price')?>" class="form-control">
                        <div class="invalid-feedback">New Plan?</div>
                        <?php echo form_error('price', '<div style="color:red">', '</div>');?>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Commission</label>
                        <input type="number" name="commission" required="" placeholder="Commission(per person)" value="<?php echo set_value('commission')?>" class="form-control">
                        <div class="invalid-feedback">Commission?</div>
                        <?php echo form_error('commission', '<div style="color:red">', '</div>');?>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Time(In sec)</label>
                        <input type="text" name="time" required="" placeholder="Time(In sec)" value="<?php echo set_value('commission')?>" class="form-control">
                        <div class="invalid-feedback">Time?</div>
                        <?php echo form_error('commission', '<div style="color:red">', '</div>');?>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" value="Apply" class="btn btn-primary mt-27 ">Submit</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="card-body">
            <div class="card">
                <div class="card-header">
                    <h4>List of Plans</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" id="tableExport" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Price</th>
                                    <th>Commission(Per Person)</th>
                                    <th>Time(In sec.)</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($plans)):?>
                                    <?php  $sno = 1; foreach ($plans as $p): ?>
                                        <tr>
                                            <td><?php echo $sno++;?></td>
                                            <td><?php echo $p['price'];?></td>
                                            <td><?php echo $p['commission'];?></td>
                                             <td><?php echo $p['time'];?></td>
                                            <td>
                                                <a href="<?php echo base_url()?>plans/edit?id=<?php echo $p['id']; ?>" class=" mr-2  " type="plans"> <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $p['id'] ?>, 'plans')">
                                                    <i class="far fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                            <?php else :?>
                                                <tr>
                                                    <th colspan='5'><h3><center>No Plans</center></h3></th>
                                                </tr>
                                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>