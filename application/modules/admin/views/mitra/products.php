<!--Add Product And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Product</h4>
		<form class="needs-validation" novalidate=""
			action="<?php echo base_url('product/c');?>" method="post"
			enctype="multipart/form-data">
			<div class="card-header">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label>Product Name</label> <input type="text"
							class="form-control" name="name" placeholder="Product Name" required="" value="<?php echo set_value('name')?>">
						<div class="invalid-feedback">New Sub_Category Name?</div>
						<?php echo form_error('name','<div style="color:red">','</div>')?>
					</div>

					<div class="form-group col-md-4">
						<label>Scheme</label>
						<!-- <input type="file" class="form-control" required="">-->
						<select class="form-control" name="product_id" required="" >
								<option value="0" selected disabled>--select--</option>
    							<?php foreach ($categories as $category):?>
    								<option value="<?php echo $category['id'];?>"><?php echo $category['name']?></option>
    							<?php endforeach;?>
						</select>
						<div class="invalid-feedback">New Scheme Name?</div>
						<?php echo form_error('cat_id','<div style="color:red>"','</div>');?>
					</div>
					<div class="form-group col-md-4">
						<label>Upload Image</label> 
						
						<input type="file" name="file" required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);">
<!-- 							<img id="blah" src="#" alt="" > -->
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>

				
        <div class="col col-md-12" >
                    <label for="type" class="col-4 col-form-label">Description</label> 
                      	<textarea id="beauty_desc" name="desc" class="ckeditor" rows="15" data-sample-short></textarea>
                      	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                    </div>
					
					<div class="form-group col-md-12">

						<button class="btn btn-primary mt-27 ">Submit</button>
					</div>


				</div>


			</div>
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Products</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Id</th>
									<th>Product Name</th>
									<th>Scheme</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
								<?php if(!empty($sub_categories)):?>
    							<?php $sno = 1; foreach ($sub_categories as $sub_cat):?>
    								<tr>
    									<td><?php echo $sno++;?></td>
    									<td><?php echo $sub_cat['name'];?></td>
    									<td><?php foreach ($categories as $category):?>
    										<?php echo ($category['id'] == $sub_cat['product_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
    									<td><?php echo $sub_cat['desc'];?></td>
    									<td width="15%"><img
    										src="<?php echo base_url();?>uploads/product_image/product_<?php echo $sub_cat['id'];?>.jpg"
    										width="50px"></td>
    									<td><a href="<?php echo base_url()?>product/edit?id=<?php echo $sub_cat['id'];?>" class=" mr-2  " type="product" > <i class="fas fa-pencil-alt"></i>
    									</a> <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $sub_cat['id'] ?>, 'product')"> <i
    											class="far fa-trash-alt"></i>
    									</a></td>
    
    								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr ><th colspan='6'><h3><center>No Products</center></h3></th></tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>

