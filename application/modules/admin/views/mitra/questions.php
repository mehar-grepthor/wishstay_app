<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4 class="col-10">List of Questions</h4>
					<a class="btn btn-outline-dark btn-lg col-2" href="<?php echo base_url('question/c')?>"><i class="fa fa-plus" aria-hidden="true"></i> Add Question</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Question</th>
									<th>Options</th>
									<th>Answer</th>
									<th>Category</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($questions)):?>
    							<?php  $sno = 1; foreach ($questions as $q): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $q['question'];?></td>
									<td>
										<ul>
											<?php if(isset($q['options'])){foreach ($q['options'] as $option){?>
												<li><?php echo $option['option'];?></li>
											<?php }}?>
										</ul>
									</td>
									<td><?php if(isset($q['options'])){
									    foreach ($q['options'] as $option){ if($option['status'] == 1){echo $option['option'];}}
									}?></td>
									<td><?php foreach ($categories as $category):?>
    									<?php echo ($category['id'] == $q['cat_id'])? $category['name']:'';?>
    									<?php endforeach;?></td>
									<td><a
										href="<?php echo base_url()?>question/edit?id=<?php echo $q['id']; ?>"
										class=" mr-2  " type="category"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $q['id'] ?>, 'question')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='6'><h3>
											<center>Questions Not Available</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>