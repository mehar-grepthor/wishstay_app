<!--Add Category And its list-->
<div class="row">
	<div class="col-12">
		<h4>Add Scheme</h4>
		  
		<form class="needs-validation" novalidate="" action="<?php echo base_url('scheme/c');?>" method="post" enctype="multipart/form-data">
			<div class="card-header">
				<div class="form-group row">
					<div class="form-group col-md-4">
						<label>Scheme Name</label> <input type="text" name="name"
							required="" placeholder="Category Name" value="<?php echo set_value('name')?>"
							class="form-control">
						<div class="invalid-feedback">New Scheme?</div>
						<?php echo form_error('name', '<div style="color:red">', '</div>');?>
					</div>
					
					<div class="form-group col-md-4">
						<label>Upload Image</label> <input type="file" name="file"
							required="" value="<?php echo set_value('file')?>"
							class="form-control" onchange="readURL(this);"> <br> <img id="blah"
							src="#" alt="">
						<div class="invalid-feedback">Upload Image?</div>
						<?php echo form_error('file', '<div style="color:red">', '</div>');?>
					</div>
					<div class="col col-md-12" >
                    <label for="type" class="col-4 col-form-label">Description</label> 
                      	<textarea id="beauty_desc" name="desc" class="ckeditor" rows="15" data-sample-short></textarea>
                      	<?php echo form_error('desc', '<div style="color:red">', '</div>');?>
                    </div>
					<div class="form-group col-md-2">
						<button type="submit" name="upload" id="upload" value="Apply"
							class="btn btn-primary mt-27 ">Submit</button>
					</div>
				</div>
</div>
			
		</form>

		<div class="card-body">
			<div class="card">
				<div class="card-header">
					<h4>List of Scheme</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover" id="tableExport"
							style="width: 100%;">
							<thead>
								<tr>
									<th>Sno</th>
									<th>Scheme Name</th>
									<th>Description</th>
									<th>Image</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>
							<?php if(!empty($categories)):?>
    							<?php  $sno = 1; foreach ($categories as $category): ?>
    								<tr>
									<td><?php echo $sno++;?></td>
									<td><?php echo $category['name'];?></td>
									<td><?php echo $category['desc'];?></td>
								
									<td><img
										src="<?php echo base_url();?>uploads/scheme_image/scheme_<?php echo $category['id'];?>.jpg"
										class="img-thumb"></td>
									<td><a
										href="<?php echo base_url()?>scheme/edit?id=<?php echo $category['id']; ?>"
										class=" mr-2  " type="scheme"> <i class="fas fa-pencil-alt"></i>
									</a> <a href="#" class="mr-2  text-danger "
										onClick="delete_record(<?php echo $category['id'] ?>, 'scheme')">
											<i class="far fa-trash-alt"></i>
									</a></td>

								</tr>
    							<?php endforeach;?>
							<?php else :?>
							<tr>
									<th colspan='5'><h3>
											<center>No Categories</center>
										</h3></th>
								</tr>
							<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
</div>
