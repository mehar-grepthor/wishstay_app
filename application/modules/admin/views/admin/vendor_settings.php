<div class="container">
    <div class="row">
        <div class="col-md-6" style="">
            <form id="form_site_settings" action="<?php echo base_url('vendor_settings/food');?>" method="post" class="needs-validation reset" novalidate="" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">Food Settings</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3">Vendor</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="vendor_id">
                                    <option value="">Select Vendor</option>
                                    <option value="all">All Vendors</option>
                                    <?php
                                    foreach ($vendors as $row) {
                                        ?>
                                        <option value="<?=$row['vendor_user_id'];?>"><?=$row['name'];?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3">Min Order Price<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="min_order_price" placeholder="Min Order Price" required="" min="1" value="<?php echo $this->vendor_settings_model->where('key', 'min_order_price')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Min Order Price ?</div>
                            <?php echo form_error('min_order_price','<div style="color:red">','</div>');?>
                                <input type="hidden" name="id" value="">
                                <br>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Delivery Free Range (Km) <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="delivery_free_range" placeholder="Delivery Free Range (Km)" required="" min="0" value="<?php echo $this->vendor_settings_model->where('key','delivery_free_range')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Delivery Free Range (Km) ?</div>
                            <?php echo form_error('delivery_free_range','<div style="color:red">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Min Delivery Fee<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="min_delivery_fee" placeholder="Min Delivery Feee" required="" value="<?php echo $this->vendor_settings_model->where('key','min_delivery_fee')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Min Delivery Fee ?</div>
                            <?php echo form_error('min_delivery_fee','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Extra Delivery Fee (per km)<span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ext_delivery_fee" placeholder="Min Delivery Feee" required="" value="<?php echo $this->vendor_settings_model->where('key','ext_delivery_fee')->get()['value'];?>">
                            </div>
                            <div class="invalid-feedback">Extra Delivery Fee (per km) ?</div>
                            <?php echo form_error('ext_delivery_fee','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Tax (in %)</label>
                            <div class="col-sm-9">
                                <input type="number" required="" class="form-control" name="tax" placeholder="Tax" value="0" min="0">
                            </div>
                            <div class="invalid-feedback">Tax ?</div>
                            <?php echo form_error('tax','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Vendor Label</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="label" placeholder="Label" value="">
                            </div>
                            <div class="invalid-feedback">Vendor Label ?</div>
                            <?php echo form_error('label','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_site_settings')" value="Reset" />
                            </div>
                        </div>

                    </div>
            
            </section>
            </form>
        </div>
        <div class="col-md-6" style="">
            <form id="form_site_settings" action="<?php echo base_url('vendor_settings/food_item_label');?>" method="post" class="needs-validation reset" novalidate="" enctype="multipart/form-data">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                        </div>
                        <h2 class="card-title">Food Items Label</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3">Vendor</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="vendor_id" onchange="get_menus_by_v(this.value);" required="">
                                    <option value="">Select Vendor</option>
                                    <?php
                                    foreach ($vendors as $row) {
                                        ?>
                                        <option value="<?=$row['vendor_user_id'];?>"><?=$row['name'];?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3">Menus</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="menu_id" id="menus_list" onchange="get_sub_item(this.value)" required="">
                                    <option value="">Select Menu</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3">Items</label>
                            <div class="col-sm-9">
                            <select class="form-control" name="item_id"  id="sub_items" required="">
                                    <option value="">Select Items</option>
                                </select>
                            </div>
                            <div class="invalid-feedback">Item?</div>
                            <?php echo form_error('item_id','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 ">Item Label</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="label" placeholder="Label" value="" required="">
                            </div>
                            <div class="invalid-feedback">Item Label ?</div>
                            <?php echo form_error('label','<div style="color:red" "margin_left=100px">','</div>');?>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <input type="button" class="btn btn-default" onClick="clear_form('form_site_settings')" value="Reset" />
                            </div>
                        </div>

                    </div>
            
            </section>
            </form>
        </div>
    </div>
</div>
<style>
    #editor{
  padding: 0.4em 0.4em 0.4em 0;

}
</style>





<script type="text/javascript">
    function get_sub_item(item_id) {
         $.ajax({
            url: '<?php echo base_url();?>food/get_sub_item_list/' + item_id ,
            type: 'get',
            success: function(response)
            {
                $('#sub_items').html(response);
            }
        });
    }
    function get_menus_by_v(vendor_id) {
         $.ajax({
            url: '<?php echo base_url();?>food/get_food_menus/' + vendor_id ,
            type: 'get',
            success: function(response)
            {
                $('#menus_list').html(response);
            }
        });
    }
</script>